# Contoh Proyek Komplit: Sistem Manajemen Lomba

## Teknologi

- Angular
- Ionic
- Firebase
- Capasitor

## intalasi

1. Cloning project ke komputer lokal Anda
2. Jalankan `npm install` menggunakan terminal atau cmd, pastikan sudah mengarah ke folder project
3. Buat project pada Firebase.
4. Aktifkan authentikasi login menggunakan email dan password pada firebase
5. Buat Firestore database
6. Atur role pada firestore database dengan pengaturan sebagai berikut:
```
rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
    match /{document=**} {
      allow read, write: if false;
    }
    match /users/{email}
    {
    	allow update: if request.auth.token.email == resource.id;
      allow create: if true;
      allow delete: if request.auth.uid != null;
      allow read: if request.auth.uid != null;
    }
    match /events/{id}{
    	allow read: if request.auth.uid != null;
      allow update, delete: if resource.data.author == request.auth.token.email;
      allow create: if request.auth.uid != null;
    }
    match /competitions/{id}{
    	allow read, create, update: if request.auth.uid != null;
      allow delete: if resource.data.author == request.auth.token.email;
      
    }
    match /partisipants/{id}{
    	allow read, write, update, delete: if request.auth.uid != null;
     
    }
    match /bills/{id}
    {
    	allow read, write, update, delete: if request.auth.uid != null;
    }
    match /articles/{id}
    {
    	allow write,read: if request.auth.uid != null;
      allow update, delete: if request.auth.token.email == resource.data.author;
    }
    match /medals/{id}
    {
    	allow read, write, update, delete: if request.auth.uid != null;
    }
  }
}

```
7. Aktifkan Web App pada firebase.
8. Setting koneksi database pada `src/environments` sesuai dengan kode yang diperoleh pada proses 7.
9. Jalankan aplikasi dengan perintah `ionic serve`
