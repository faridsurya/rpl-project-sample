import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(
    public toastController: ToastController,
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public router: Router
  ) { }

  ngOnInit() {
  }

  async pesanKesalahan() {
    const toast = await this.toastController.create({
      message: 'Tidak dapat melakukan registrasi. Coba lagi!',
      duration: 2000,
      position:'middle'
    });
    toast.present();
  }

  user: any = {};
  loading: boolean;
  registrasi()
  {
    this.loading = true;
    //this.pesanKesalahan();
    this.auth.createUserWithEmailAndPassword(this.user.email, this.user.password).then(res=>{
      this.updateUserData(res.user.email);
    }).catch(err=>{
      this.loading = false;
      this.pesanKesalahan();
    })
  }

  updateUserData(email)
  {
    this.auth.onAuthStateChanged(res=>{
      res.updateProfile({displayName:this.user.name});
      this.createUserData(email);
    });
  }

  createUserData(email)
  {
    var dt = {
      name: this.user.name
    };
    this.db.collection('users').doc(email).set(dt).then(res=>{
      this.loading = false;
      this.router.navigate(['app/tabs/tab1']);
    }).catch(err=>{
      this.loading = false;
      this.pesanKesalahan();
    })
  }

 

  

}
