import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    public router: Router,
    public auth: AngularFireAuth
  ) { }

  ngOnInit() {
    this.checkLogin();
  }

  user: any = {};
  msg: any;
  loading: boolean;
  doLogin()
  {
    this.loading = true;
    this.auth.signInWithEmailAndPassword(this.user.email, this.user.password).then(res=>{
      this.loading = false;
      this.router.navigate(['app/tabs/tab1']);
    }).catch(err=>{
      this.loading = false;
      this.msg = 'Tidak dapat login. Pastikan anda sudah melakukan registrasi pada aplikasi ini.';
      setTimeout(()=>{this.msg = null;},4000);   
    });    
  }

  checkLogin()
  {
    this.auth.onAuthStateChanged(user=>{
      if(user.uid != null) this.router.navigate(['app/tabs/tab1']);
      return;
    })
  }

}
