import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'app',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path:'',
    redirectTo:'login',
    pathMatch:'full'
  },
  {
    path: 'register',
    loadChildren: () => import('./auth/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'forgot',
    loadChildren: () => import('./auth/forgot/forgot.module').then( m => m.ForgotPageModule)
  },
  {
    path: 'form-kegiatan/:ID',
    loadChildren: () => import('./form-kegiatan/form-kegiatan.module').then( m => m.FormKegiatanPageModule)
  },
  {
    path: 'daftar-lomba/:ID',
    loadChildren: () => import('./lomba/daftar-lomba/daftar-lomba.module').then( m => m.DaftarLombaPageModule)
  },
  {
    path: 'detail-peserta',
    loadChildren: () => import('./lomba/detail-peserta/detail-peserta.module').then( m => m.DetailPesertaPageModule)
  },
  {
    path: 'detail-lomba/:ID',
    loadChildren: () => import('./lomba/detail-lomba/detail-lomba.module').then( m => m.DetailLombaPageModule)
  },
  {
    path: 'form-pendaftaran',
    loadChildren: () => import('./lomba/form-pendaftaran/form-pendaftaran.module').then( m => m.FormPendaftaranPageModule)
  },
  {
    path: 'form-data-lomba',
    loadChildren: () => import('./lomba/form-data-lomba/form-data-lomba.module').then( m => m.FormDataLombaPageModule)
  },
  {
    path: 'topnav',
    loadChildren: () => import('./topnav/topnav.module').then( m => m.TopnavPageModule)
  },
  {
    path: 'image-uploder',
    loadChildren: () => import('./image-uploder/image-uploder.module').then( m => m.ImageUploderPageModule)
  },
  {
    path: 'image-viewer',
    loadChildren: () => import('./media/image-viewer/image-viewer.module').then( m => m.ImageViewerPageModule)
  },
  {
    path: 'payment-detail/:ID',
    loadChildren: () => import('./payment/payment-detail/payment-detail.module').then( m => m.PaymentDetailPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profiles/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'email',
    loadChildren: () => import('./profiles/email/email.module').then( m => m.EmailPageModule)
  },
  {
    path: 'password',
    loadChildren: () => import('./profiles/password/password.module').then( m => m.PasswordPageModule)
  },
  {
    path: 'article-writer/:ID',
    loadChildren: () => import('./article/article-writer/article-writer.module').then( m => m.ArticleWriterPageModule)
  },
  {
    path: 'email',
    loadChildren: () => import('./profiles/email/email.module').then( m => m.EmailPageModule)
  },
  {
    path: 'password',
    loadChildren: () => import('./profiles/password/password.module').then( m => m.PasswordPageModule)
  },
  {
    path: 'guide-writer/:ID',
    loadChildren: () => import('./lomba/guide-writer/guide-writer.module').then( m => m.GuideWriterPageModule)
  },
  {
    path: 'frame-foto',
    loadChildren: () => import('./alat/frame-foto/frame-foto.module').then( m => m.FrameFotoPageModule)
  },
  {
    path: 'pilih-frame',
    loadChildren: () => import('./alat/pilih-frame/pilih-frame.module').then( m => m.PilihFramePageModule)
  }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
