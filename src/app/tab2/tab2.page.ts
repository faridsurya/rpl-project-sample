import { Component, OnInit } from '@angular/core';
import * as domtoimage from 'dom-to-image';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  constructor(
    public auth: AngularFireAuth,
    public db: AngularFirestore
  ) {}

  ngOnInit()
  {
    this.auth.onAuthStateChanged(user=>{
      this.getData();
    });
  }

  users: any = [];
  getData()
  {
    this.db.collection('users').valueChanges({idField: 'id'}).subscribe(res=>{
      this.users = res;
    });
  }

  downloadedImg: any;
  toDataURL(url) {    
    this.downloadedImg = new Image;
    this.downloadedImg.crossOrigin = "Anonymous";
    //downloadedImg.addEventListener("load", imageReceived, false);
    this.downloadedImg.src = url;
  }

  getBase64Image(img) {
    
    var canvas = document.createElement("canvas");
    //canvas.width = img.width;
    //canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    console.log(dataURL);
  }


  parseEvents(data)
  {
    for(var i=0; i<data.length; i++)
    {
      var xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = function(event) {
      var blob = xhr.response;
      };
      xhr.open('GET', data[i].thumbnail.url);
      xhr.send();
    }
  }

  exportPng()
  {
    domtoimage.toJpeg(document.getElementById('img'), { quality: 1 })
    .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'my-image-name.jpeg';
        link.href = dataUrl;
        link.click();
    });
  }

}
