import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastService } from 'src/app/services/toast.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';


@Component({
  selector: 'app-email',
  templateUrl: './email.page.html',
  styleUrls: ['./email.page.scss'],
})
export class EmailPage implements OnInit {

  @Input() user: any;
  @Input() email: any;
  constructor(
    public modalCtrl: ModalController,
    public db: AngularFirestore,
    public fsAuth: AngularFireAuth,
    public toast: ToastService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  userAuth:any;
  tempEmail:any;
  ionViewDidEnter() {
    this.fsAuth.onAuthStateChanged((user) => {
      if (user) {
        this.userAuth = user;
        this.tempEmail = this.userAuth.email;
        this.db.collection('users').doc(this.userAuth.email).valueChanges().subscribe( res =>{
        })
      }
    })
  }

  loading:boolean;
  saveData()
  {
    this.loading = true;
    this.userAuth.updateEmail(this.email).then(res => {
      this.loading = false;
      this.db.collection('users').doc(this.email).set(this.user).then( res =>{
        this.deleteDoc(this.tempEmail);
      });
    }).catch(err=>{
      this.loading = false;
      this.toast.present('Tidak dapat menyimpan data, coba lagi.','top');
    })
  }

  
  deleteDoc(email) {
    this.db.collection('users').doc(email).delete().then( res =>{
      this.dismiss();
      this.fsAuth.signOut();
      this.router.navigate(['/login']);
    });
  }

  dismiss()
  {
    this.modalCtrl.dismiss();
  }

}
