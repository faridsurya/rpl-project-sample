import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastService } from 'src/app/services/toast.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';


@Component({
  selector: 'app-password',
  templateUrl: './password.page.html',
  styleUrls: ['./password.page.scss'],
})
export class PasswordPage implements OnInit {

  @Input() user: any;
  @Input() email: any;
  constructor(
    public modalCtrl: ModalController,
    public db: AngularFirestore,
    public fsAuth: AngularFireAuth,
    public toast: ToastService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  userAuth:any;
  ionViewDidEnter() {
    this.fsAuth.onAuthStateChanged((user) => {
      if (user) {
        this.userAuth = user;
      }
    })
  }

  loading:boolean;
  data:any = {};
  peringatan:boolean;
  cek() {
    if(this.data.password != this.data.password2) {
      this.peringatan = true;
    } else if(this.data.password == this.data.password2) {
      this.peringatan = false;
    }
  }

  saveData()
  {
    this.loading = true;
    this.userAuth.updatePassword(this.data.password).then(res => {
      this.loading = false;
      this.dismiss();
      this.fsAuth.signOut();
      this.router.navigate(['/login']);
    }).catch(err=>{
      this.loading = false;
      this.toast.present('Tidak dapat menyimpan data, coba lagi.','top');
    })
  }

  dismiss()
  {
    this.modalCtrl.dismiss();
  }

}
