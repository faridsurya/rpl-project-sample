import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TopnavPage } from './topnav.page';

describe('TopnavPage', () => {
  let component: TopnavPage;
  let fixture: ComponentFixture<TopnavPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopnavPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TopnavPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
