import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TopnavPage } from './topnav.page';

const routes: Routes = [
  {
    path: '',
    component: TopnavPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TopnavPageRoutingModule {}
