import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TopnavPageRoutingModule } from './topnav-routing.module';

import { TopnavPage } from './topnav.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TopnavPageRoutingModule
  ],
  declarations: [TopnavPage]
})
export class TopnavPageModule {}
