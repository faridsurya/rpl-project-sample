import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormKegiatanPage } from './form-kegiatan.page';

describe('FormKegiatanPage', () => {
  let component: FormKegiatanPage;
  let fixture: ComponentFixture<FormKegiatanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormKegiatanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormKegiatanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
