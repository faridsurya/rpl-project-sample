import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormKegiatanPageRoutingModule } from './form-kegiatan-routing.module';

import { FormKegiatanPage } from './form-kegiatan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormKegiatanPageRoutingModule
  ],
  declarations: [FormKegiatanPage]
})
export class FormKegiatanPageModule {}
