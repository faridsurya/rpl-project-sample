import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormKegiatanPage } from './form-kegiatan.page';

const routes: Routes = [
  {
    path: '',
    component: FormKegiatanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormKegiatanPageRoutingModule {}
