import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastService } from '../services/toast.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage';
import { ImageViewerPage } from '../media/image-viewer/image-viewer.page';

import { Plugins, CameraResultType } from '@capacitor/core';
import { ImageUploderPage } from '../image-uploder/image-uploder.page';
const { Camera } = Plugins

@Component({
  selector: 'app-form-kegiatan',
  templateUrl: './form-kegiatan.page.html',
  styleUrls: ['./form-kegiatan.page.scss'],
})
export class FormKegiatanPage implements OnInit {

  constructor(
    public db: AngularFirestore,
    public auth: AngularFireAuth,
    public toast: ToastService,
    public routes: ActivatedRoute,
    public actionSheetController: ActionSheetController,
    public storage: AngularFireStorage,
    public modalController: ModalController,
    public router: Router
  ) { }
  
  eventID: any;
  userdata: any = {};
  data: any = {committees:[],payment:{}};
  selectedSegment: any = 'informasi';
  ngOnInit() {
    this.eventID = this.routes.snapshot.paramMap.get('ID');
    this.auth.onAuthStateChanged(user=>{
      this.userdata = user;
      if(this.eventID != 0){
        this.getData();
        this.getBills();
      }
    });
  }

  
  getData()
  {
    this.db.collection('events').doc(this.eventID).valueChanges().subscribe(res=>{
      this.data = res;
    })
  }

  //payment
  payments:any=[];
  getBills()
  {
    this.db.collection('bills',ref=>{
      return ref.where('eventID','==',this.eventID);
    }).valueChanges({idField:'id'}).subscribe(res=>{
      this.payments = res;
      this.parseBillData(res);
    })
  }
  //end payment

  parseBillData(data)
  {
    for(var i=0; i<data.length; i++)
    {
      this.getUser(data[i].author);
    }
  }

  users:any={};
  getUser(email)
  {
      this.db.collection('users').doc(email).get().subscribe(res=>{
        this.users[email] = res.data();
      })
  }

  loading:boolean;
  simpanKegiatan()
  {
    this.loading = true;
    this.data.author = this.userdata.email;    
    var doc;
    if(this.eventID == 0)    
    doc = new Date().getTime().toString();
    else doc = this.eventID;
    
    this.db.collection('events').doc(doc).set(this.data).then(res=>{
      this.loading = false;
      this.toast.present('Penyimpanan berhasil','middle');
    });
    
  }

  email: any;
  loadingAddComm : boolean;
  addCommittees(email)
  {
    this.loadingAddComm = true;
    var idx = this.data.committees.indexOf(email);
    this.db.collection('users').doc(email).get().subscribe(res=>{
      if(res.data() == undefined)
      {
        this.toast.present('Email tidak terdaftar','middle');
      }else{
        if(idx == -1)
        this.data.committees.push(email);
        else this.toast.present('Email sudah tersedia','middle');
      };
      this.loadingAddComm = false;
      this.email = '';
    },err=>{
      this.loadingAddComm = false;
      this.toast.present('Tidak dapat menambahkan. Coba lagi!','middle');
    })
    
  }

  async presentActionSheet(data) {
    var buttons = [
      {
        text: 'Lihat Detail Pembayaran',
        icon: 'image-outline',
        handler: () => {
         this.router.navigate(['payment-detail/'+data.id]);
        }
      },
      {
      text: 'Terima Pembayaran',
      icon: 'cash-outline',
      cssClass:'disable-action-sheet-btns',
      handler: () => {
        var conf = confirm('Yakin terima pembayaran?');
        if(conf == false) return;
        this.approvePartisipants(data.bill_items,data.id);
      }
    }, {
      text: 'Batalkan Pembayaran',
      icon: 'backspace-outline',
      handler: () => {
        var conf = confirm('Yakin batalkan pembayaran?');
        if(conf == false) return;
        this.cancelPaymentPartisipants(data.bill_items,data.id);
        
      }
    },{
      text: 'Cancel',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }
    ];

    //control button visibility
    if(data.status == 'canceled') return alert('Konfirmasi pembayaran sudah dibatalkan');
    if(data.status == 'approved'){      
      buttons.splice(1,2);
    } 

    const actionSheet = await this.actionSheetController.create({
      header: 'Pilih Tindakan untuk INV'+data.id,
      cssClass: 'my-custom-class',
      buttons: buttons
    });
    await actionSheet.present();
  }

  updateBill(doc, data,msg)
  {
    this.db.collection('bills').doc(doc).update(data).then(res=>{
      this.toast.present(msg,'middle');
    })
  }

  async showImage(data)
  {
    const modal = await this.modalController.create({
      component: ImageViewerPage,
      cssClass: 'my-custom-class',
      componentProps:{imageData: data.receipt.url, title: 'Bukti Bayar'}
    });
    return modal.present();
  }

  approvePartisipants(data,doc)
  {
    for(var i=0; i<data.length; i++)
    {
      this.db.collection('partisipants').doc(data[i]).update({valid:true,bill_id:doc});
    }
    this.updateBill(doc,{status:'approved'},'Pembayaran berhasil dikonfirmasi');
  }

  cancelPaymentPartisipants(data,doc)
  {
    for(var i=0; i<data.length; i++)
    {
      this.db.collection('partisipants').doc(data[i]).update({processing_payment: false});
    }
    this.updateBill(doc,{status:'canceled'},'Pembayaran berhasil dibatalkan');
  }

  async takePicture(mode) {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.DataUrl
    });
    var imageUrl = image.dataUrl;
    var imagePath = this.eventID+'/logo.png';
    var ratio = 1, width = 200, height = 200;
    if(mode === 0){
      ratio = 4/3;
      imagePath = this.eventID+'/thumbnail.png';
      width = 400;
      height = 300;
    }
    const modal = await this.modalController.create({
      component: ImageUploderPage,
      cssClass: 'my-custom-class',
      componentProps:{imageData:imageUrl,imagePath: imagePath, ratio:ratio,widht:width, height:height}
    }); 
    modal.onDidDismiss().then(res=>{
      if(res.data.imageUrl != false)
      {
        //do action for imageUrl
        if(mode === 0){
          this.db.collection('events').doc(this.eventID).update({
            thumbnail:{ref:imagePath,url:res.data.imageUrl}
          })
        }else{
          this.db.collection('events').doc(this.eventID).update({
            logo:{ref:imagePath,url:res.data.imageUrl}
          })
        }

      }
    });
    return await modal.present();
}

}
