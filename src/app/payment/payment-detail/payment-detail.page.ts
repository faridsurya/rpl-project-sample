import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ImageUploderPage } from 'src/app/image-uploder/image-uploder.page';
import { ModalController } from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage';
import { Plugins, CameraResultType } from '@capacitor/core';
const { Camera } = Plugins;

@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.page.html',
  styleUrls: ['./payment-detail.page.scss'],
})
export class PaymentDetailPage implements OnInit {
  ID: any;
  constructor(
    public routes: ActivatedRoute,
    public db: AngularFirestore,
    public auth: AngularFireAuth,
    public modalController: ModalController,
    public storage: AngularFireStorage
  ) { }

  userdata: any = {};
  ngOnInit() {
    this.ID = this.routes.snapshot.paramMap.get('ID');
    this.auth.onAuthStateChanged(res=>{
      this.userdata = res;
      this.getData();
    });
  }

  data: any = {};
  getData()
  {
    this.db.collection('bills').doc(this.ID).valueChanges().subscribe(res=>{
      this.data = res;
      this.getEventData(res);
    })
  }

  event: any = {payment:{}};
  getEventData(data)
  {
    this.db.collection('events').doc(data.eventID).get().subscribe(res=>{
      this.event = res.data();
    })
  }

   //take photo
   async takePicture(mode) {
    const image = await Camera.getPhoto({
      quality: 75,
      allowEditing: false,
      resultType: CameraResultType.DataUrl
    });
    var imageUrl = image.dataUrl;
    var imagePath = this.userdata.email+'/'+ new Date().getTime()+'.png';
    //delete old pic
    if(mode==1)
    {
      this.storage.ref(this.data.receipt.ref).delete();
    }
    const modal = await this.modalController.create({
      component: ImageUploderPage,
      cssClass: 'my-custom-class',
      componentProps:{imageData:imageUrl,imagePath: imagePath}
    }); 
    modal.onDidDismiss().then(res=>{
      if(res.data.imageUrl != false)
      {
        this.updateBill(res.data.imageUrl,imagePath);
      }
    });
    return await modal.present();

  }

  updateBill(url,path)
  {
    this.db.collection('bills').doc(this.ID).update({
      receipt:{ref:path,url:url}
    });
  }

}
