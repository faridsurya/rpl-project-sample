import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuideWriterPage } from './guide-writer.page';

const routes: Routes = [
  {
    path: '',
    component: GuideWriterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GuideWriterPageRoutingModule {}
