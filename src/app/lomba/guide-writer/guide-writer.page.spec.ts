import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GuideWriterPage } from './guide-writer.page';

describe('GuideWriterPage', () => {
  let component: GuideWriterPage;
  let fixture: ComponentFixture<GuideWriterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuideWriterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GuideWriterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
