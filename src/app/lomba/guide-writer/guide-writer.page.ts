import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore} from '@angular/fire/firestore';
import { AngularEditorConfig } from '@kolkov/angular-editor';



@Component({
  selector: 'app-guide-writer',
  templateUrl: './guide-writer.page.html',
  styleUrls: ['./guide-writer.page.scss'],
})
export class GuideWriterPage implements OnInit {

  constructor(
    private routes: ActivatedRoute,
    private db: AngularFirestore,
    private router: Router
  ) { }
  
  competitionID: any;
  ngOnInit() {
    this.competitionID = this.routes.snapshot.paramMap.get('ID');
    this.getData();
  }
  content: any;
  getData()
  {
    this.db.collection('competitions').doc(this.competitionID).get().subscribe(res=>{
      this.getContent(res.data());
    })
  }
  getContent(data)
  {
    if(data.guide == undefined) this.content = '';
    else this.content = data.guide;
  }
  loading: boolean;
  saveData()
  {
    this.loading = true;
    this.db.collection('competitions').doc(this.competitionID).update({
      guide: this.content
    }).then(res=>{
      this.loading = false;
      this.router.navigate(['detail-lomba/'+this.competitionID]);
    })
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: 'p',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'bottom',
    toolbarHiddenButtons: [
      [
        'undo',
        'redo',
        //'bold',
        //'italic',
        //'underline',
        'strikeThrough',
        'subscript',
        'superscript',
        'justifyLeft',
        'justifyCenter',
        'justifyRight',
        'justifyFull',
        'indent',
        'outdent',
        'insertUnorderedList',
        //'insertOrderedList',
        //'heading',
        'fontName'
      ],
      [
        'fontSize',
        'textColor',
        'backgroundColor',
        'customClasses',
        'link',
        'unlink',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode'
      ]
    ]
  };

}
