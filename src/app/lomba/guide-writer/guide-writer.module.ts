import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule} from '@angular/common/http';
import { GuideWriterPageRoutingModule } from './guide-writer-routing.module';

import { GuideWriterPage } from './guide-writer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GuideWriterPageRoutingModule,
    AngularEditorModule,
    HttpClientModule
  ],
  declarations: [GuideWriterPage]
})
export class GuideWriterPageModule {}
