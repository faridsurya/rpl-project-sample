import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastService } from 'src/app/services/toast.service';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-form-pendaftaran',
  templateUrl: './form-pendaftaran.page.html',
  styleUrls: ['./form-pendaftaran.page.scss'],
})
export class FormPendaftaranPage implements OnInit {
  @Input() eventID: string;
  constructor(
    public modalCtrl: ModalController,
    public db: AngularFirestore,
    public auth: AngularFireAuth,
    public toast: ToastService
  ) { }
  
  userdata: any = {};
  ngOnInit() {
    this.auth.onAuthStateChanged(user=>{
      this.userdata = user;
    });
  }

  data: any = {};
  
  //get competition
  competitions: any = [];
  getCompetitions()
  {
    this.db.collection('competitions',ref=>{
      return ref.where('eventID','==',this.eventID)
      .where('level','==',this.data.level);
    }).valueChanges({idField:'id'}).subscribe(res=>{
      this.competitions = res;
    })
  }

  loading: boolean;
  addPartisipant()
  {
    this.loading = true;
    this.data.eventID = this.eventID;
    this.data.author = this.userdata.email;
    this.data.processing_payment = false;
    this.data.score = 0;
    var doc = new Date().getTime().toString();
    this.db.collection('partisipants').doc(doc).set(this.data).then(res=>{
      this.toast.present('Peserta berhasil ditambahkan','middle');      
      this.getDetailCompetition(doc);      
    }).catch(err=>{
      this.toast.present('Tidak dapat menambahkan peserta','middle');
      this.loading = false;
    })
  }

  getDetailCompetition(doc)
  {
    this.db.collection('competitions').doc(this.data.competitionID).get().subscribe(res=>{
     this.updateTotalPartisipant(res.data(),doc);
    })
  }

  updateTotalPartisipant(data,doc)
  {    
    this.db.collection('competitions').doc(this.data.competitionID).update({
      partisipants: firebase.default.firestore.FieldValue.arrayUnion(doc)
    }).then(res=>{
      this.dismiss();
      this.loading = false;
    })
  }


  

  dismiss() {    
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
