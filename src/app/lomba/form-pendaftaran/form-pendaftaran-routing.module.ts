import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormPendaftaranPage } from './form-pendaftaran.page';

const routes: Routes = [
  {
    path: '',
    component: FormPendaftaranPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormPendaftaranPageRoutingModule {}
