import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormPendaftaranPage } from './form-pendaftaran.page';

describe('FormPendaftaranPage', () => {
  let component: FormPendaftaranPage;
  let fixture: ComponentFixture<FormPendaftaranPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPendaftaranPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormPendaftaranPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
