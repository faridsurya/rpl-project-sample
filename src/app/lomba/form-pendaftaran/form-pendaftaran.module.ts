import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormPendaftaranPageRoutingModule } from './form-pendaftaran-routing.module';

import { FormPendaftaranPage } from './form-pendaftaran.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormPendaftaranPageRoutingModule
  ],
  declarations: [FormPendaftaranPage]
})
export class FormPendaftaranPageModule {}
