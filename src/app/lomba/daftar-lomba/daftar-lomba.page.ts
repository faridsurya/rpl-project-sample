import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DetailPesertaPage } from '../detail-peserta/detail-peserta.page';
import { ModalController } from '@ionic/angular';
import { FormPendaftaranPage } from '../form-pendaftaran/form-pendaftaran.page';
import { FormDataLombaPage } from '../form-data-lomba/form-data-lomba.page';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastService } from 'src/app/services/toast.service';
import { CameraService } from 'src/app/services/camera.service';
import { ImageViewerPage } from 'src/app/media/image-viewer/image-viewer.page';



@Component({
  selector: 'app-daftar-lomba',
  templateUrl: './daftar-lomba.page.html',
  styleUrls: ['./daftar-lomba.page.scss'],
})
export class DaftarLombaPage implements OnInit {

  constructor(
    public router: Router,
    public modalController: ModalController,
    public routes: ActivatedRoute,
    public db: AngularFirestore,
    public auth: AngularFireAuth,
    public toast: ToastService,
    public picture: CameraService
  ) { }

  eventID: any;
  filterLevel: any = 'sd';
  selectedRankLevel: any = 'sd';
  userdata: any = {};
  isCommittee: boolean;
  ngOnInit() {
    this.eventID = this.routes.snapshot.paramMap.get("ID");    
    this.auth.onAuthStateChanged(res=>{
      this.userdata=res;
      this.getMyPartisipants();
      this.getPartisipants();
      this.getCompetitions();
      this.getAllCompetitions();
      this.getEvent();
      this.getMyBill();
      this.getUserDetail();
    });   
  };

  userDetail: any = {};
  getUserDetail()
  {
    this.db.collection('users').doc(this.userdata.email).get().subscribe(res=>{
      this.userDetail = res.data();
    });
  }

  //hitung peringkat

  partisipants: any = [];
  getPartisipants()
  {
    this.db.collection('partisipants',ref=>{
      return ref.where('medal','!=',null);
    }).valueChanges({idField:'id'}).subscribe(res=>{
      this.parsePartisipants(res);
    });
  }

  
  usersRegistered: any = [];
  parsePartisipants(data)
  {
    this.usersRegistered = [];
    for(var i=0; i<data.length; i++)
    {
      var dt = {
        id: data[i].author,
        level: data[i].level  
      };
      var exist = this.usersRegistered.map(e=>{return e.id}).indexOf(data[i].author);
      if( exist == -1)
      {
        dt[data[i].medal] = [data[i].id];        
        this.usersRegistered.push(dt);
      }else{
        if(  this.usersRegistered[exist][data[i].medal] == undefined )
        {
          this.usersRegistered[exist][data[i].medal] = [];
        }
        this.usersRegistered[exist][data[i].medal].push(data[i].id);
      }
      if(this.users[data[i].author] == undefined) this.getUser(data[i].author);
    }
    //console.log(this.usersRegistered);
  }

  users: any = {};
  getUser(email)
  {
    this.db.collection('users').doc(email).get().subscribe(res=>{
      this.users[email] = res.data();
    })
  }

 

  //get event data
  event:any = {};
  getEvent()
  {
    this.db.collection('events').doc(this.eventID).valueChanges().subscribe(res=>{
      this.event = res;
      this.setUserRole(res);
    })
  }
  //setup user role
  setUserRole(data)
  {
    if(data.committees != undefined && data.committees.includes(this.userdata.email))
    this.isCommittee = true;   
  }
  //get competition data
  competitions: any = [];  
  getCompetitions()
  {
    this.db.collection('competitions',ref=>{
      return ref.where('eventID','==',this.eventID)
      .where('level','==',this.filterLevel);
    }).valueChanges({idField:'id'}).subscribe(res=>{     
      this.competitions = res;
    })
  }

  competitionData: any = {};
  getAllCompetitions()
  {
    this.db.collection('competitions').valueChanges({idField:'id'}).subscribe(res=>{     
      res.forEach(val=>{
        this.competitionData[val.id] = val;
      })
    })
  }


  //get my partisipants
  myPartisipants: any = [];
  totalUnpaid: number = 0;
  totalPrice: number = 0;
  billItems: any = [];
  billDetail: any = {};
  selectedPartisipants:any = {};  
  getMyPartisipants()
  {    
    this.db.collection('partisipants',ref=>{
      return ref.where('author','==',this.userdata.email)
      .where('eventID','==',this.eventID);
    }).valueChanges({idField:'id'}).subscribe(res=>{
      this.myPartisipants = res;
      this.parseMyPartisipants(res);
      this.updateUserStatistic(res);
    })
  }

  updateUserStatistic(data)
  {
    var dt = {};
    if(this.userDetail.events == undefined )
    {
      dt['events'] = [this.eventID];
      dt['events_detail'] = {};
      dt['events_detail'][this.eventID] = {
        total_partisipants:data.length
      };
    }else{
      if(this.userDetail.events.includes(this.eventID))
      {
        dt = this.userDetail['events_detail'];
        dt[this.eventID]['total_partisipants'] = data.length;
      }
    }   
    this.db.collection('users').doc(this.userdata.email).update(dt);
  }

  parseMyPartisipants(data)
  {
    //this.myPartisipants = [];
    for(var i = 0; i < data.length; i++)
    {
      if(data[i].paid == undefined) this.totalUnpaid+=1;
      //cek
      
    }
    //this.myPartisipants = data;
  }
  calculatePrice(data)
  {
    //console.log(this.selectedPartisipants);
    //console.log(this.competitionData[data.competitionID].price);
    var bill = [];
    this.billItems = [];
    this.billDetail = {};
    //billing detail
    this.billDetail[data.id] = {
      name: data.name,
      competition:this.competitionData[data.competitionID].name,
      price: this.competitionData[data.competitionID].price
    };
    //get keys
    var keys = Object.keys(this.selectedPartisipants);
    for(var i = 0; i<keys.length; i++)
    {
      if(this.selectedPartisipants[keys[i]])
      {
        var idx = this.myPartisipants.map(e=>{return e.id}).indexOf(keys[i]);
        var price = this.competitionData[this.myPartisipants[idx].competitionID].price
        bill.push(Number(price));
        this.billItems.push(keys[i]);        
      }else{
        delete this.billDetail[keys[i]];
      }
    }
    if(bill.length > 0)
    this.totalPrice = bill.reduce(this.sum);
    else{
      this.totalPrice = 0;
      this.totalUnpaid = 0;
    }    
  }

  sum(total, num) {
    return total + num;
  }

  //billing
 
  loadingCreateBill:boolean;
  createBill()
  {
    var conf = confirm('Yakin akan membuat tagihan?');
    if(!conf) return;
    var billData = {
      author: this.userdata.email,
      amount: this.totalPrice,
      bill_items: this.billItems,
      bill_detail: this.billDetail,
      date_created: new Date(),
      eventID: this.eventID
    };
    this.loadingCreateBill = true;
    var doc = new Date().getTime().toString();
    this.db.collection('bills').doc(doc).set(billData).then(res=>{
      this.loadingCreateBill = false;
      this.updatePartisipantPaymentStatus(this.billItems);
      this.totalPrice = 0;
      this.billItems = [];
      this.selectedPartisipants = {};
      this.toast.present("Tagihan berhasil dibuat",'middle');
    });
  }

  updatePartisipantPaymentStatus(data)
  {
    for(var i=0; i<data.length; i++)
    {
      this.db.collection('partisipants').doc(data[i]).update({processing_payment: true});
    }
  }

  

  //get billing
  myBills: any = [];
  onprocessBill: any = {};
  getMyBill()
  {
    this.db.collection('bills',ref=>{
      return ref
      .where('author','==',this.userdata.email);
    }).valueChanges({idField:'id'}).subscribe(res=>{
      this.myBills = res;
      this.parseMyBill(res);
    })
  }
  parseMyBill(data)
  {
    for(var i=0; i<data.length; i++)
    {
      if(data[i].paid == undefined) this.onprocessBill = data[i];
    }
    console.log(this.onprocessBill);
  }

  checkBill(id)
  {
    if(this.onprocessBill.partisipants != undefined)
    {
      return (this.onprocessBill.partisipants.includes(id)) ? true : false;
    }else{
      return null;
    }    
  }

 
 
  async showImage(url)
  {
    const modal = await this.modalController.create({
      component: ImageViewerPage,
      cssClass: 'my-custom-class',
      componentProps:{imageData:url,title: 'Bukti Bayar'}
    });    
    return await modal.present();
  }
  

  selectedSegment:any = 'lomba';
  async detailPeserta(data) {    
    const modal = await this.modalController.create({
      component: DetailPesertaPage,
      cssClass: 'my-custom-class',
      componentProps:{data: data, competitionData: this.competitionData[data.competitionID]}
    });
    return await modal.present();
  }

  async addPartisipant() {
    const modal = await this.modalController.create({
      component: FormPendaftaranPage,
      cssClass: 'my-custom-class',
      componentProps:{
        eventID:this.eventID        
      }
    });
    return await modal.present();
  }

  async aturLomba() {
    const modal = await this.modalController.create({
      component: FormDataLombaPage,
      cssClass: 'my-custom-class',
      componentProps: {eventID: this.eventID}
    });
    return await modal.present();
  }

  eventConfig()
  {

  }


}
