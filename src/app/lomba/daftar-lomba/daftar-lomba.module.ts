import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaftarLombaPageRoutingModule } from './daftar-lomba-routing.module';

import { DaftarLombaPage } from './daftar-lomba.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaftarLombaPageRoutingModule
  ],
  declarations: [DaftarLombaPage]
})
export class DaftarLombaPageModule {}
