import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaftarLombaPage } from './daftar-lomba.page';

const routes: Routes = [
  {
    path: '',
    component: DaftarLombaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaftarLombaPageRoutingModule {}
