import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailPesertaPageRoutingModule } from './detail-peserta-routing.module';

import { DetailPesertaPage } from './detail-peserta.page';
import { YouTubePlayerModule } from '@angular/youtube-player';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailPesertaPageRoutingModule,
    YouTubePlayerModule
  ],
  declarations: [DetailPesertaPage]
})
export class DetailPesertaPageModule {}
