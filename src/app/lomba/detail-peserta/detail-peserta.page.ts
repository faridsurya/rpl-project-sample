import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

let apiLoaded = false;

@Component({
  selector: 'app-detail-peserta',
  templateUrl: './detail-peserta.page.html',
  styleUrls: ['./detail-peserta.page.scss'],
})
export class DetailPesertaPage implements OnInit {
  @Input() data: any;
  @Input() competitionData: any;
  @ViewChild('player') player: any;
  videoId: string;
  

  constructor(
    public modalCtrl: ModalController,
    public db: AngularFirestore,
    public auth: AngularFireAuth
  ) { }
  
  userdata: any = {};
  isPic:boolean;
  isAuthor:boolean;
  ngOnInit() {
    const tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    document.body.appendChild(tag);
    if(this.data.youtube_url != undefined){
      var split = this.data.youtube_url.split('v=');
      this.videoId = split[1];
    };    
    this.auth.onAuthStateChanged(res=>{
      this.userdata = res;
      if(this.competitionData.pic!= undefined && this.competitionData.pic.includes(res.email)) this.isPic = true;
      if(this.data.author == res.email) this.isAuthor = true;
      this.getEvent();
    })
  }

  //get event
  event: any = {};
  getEvent()
  {
    this.db.collection('events').doc(this.competitionData.eventID).get().subscribe(res=>{
      this.event = res.data();
    })
  }
  
  youtubeUrl:any;
  loading:boolean;
  updateData()
  {
    this.loading = true;
    this.db.collection('partisipants').doc(this.data.id).update({
      youtube_url:this.data.youtube_url
    }).then(res=>{
      this.loading = false;
    })
  }

  loadingScore:boolean;
  updateScore()
  {
    this.loadingScore = true;
    this.db.collection('partisipants').doc(this.data.id).update({
      score:this.data.score
    }).then(res=>{
      this.loadingScore = false;
    })
  }

  dismiss() {    
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
