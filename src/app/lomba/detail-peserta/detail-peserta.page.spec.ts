import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailPesertaPage } from './detail-peserta.page';

describe('DetailPesertaPage', () => {
  let component: DetailPesertaPage;
  let fixture: ComponentFixture<DetailPesertaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPesertaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailPesertaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
