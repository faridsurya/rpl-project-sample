import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailPesertaPage } from './detail-peserta.page';

const routes: Routes = [
  {
    path: '',
    component: DetailPesertaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailPesertaPageRoutingModule {}
