import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormDataLombaPageRoutingModule } from './form-data-lomba-routing.module';

import { FormDataLombaPage } from './form-data-lomba.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormDataLombaPageRoutingModule
  ],
  declarations: [FormDataLombaPage]
})
export class FormDataLombaPageModule {}
