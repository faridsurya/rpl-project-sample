import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormDataLombaPage } from './form-data-lomba.page';

const routes: Routes = [
  {
    path: '',
    component: FormDataLombaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormDataLombaPageRoutingModule {}
