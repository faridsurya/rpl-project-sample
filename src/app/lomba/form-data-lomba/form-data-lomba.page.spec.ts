import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormDataLombaPage } from './form-data-lomba.page';

describe('FormDataLombaPage', () => {
  let component: FormDataLombaPage;
  let fixture: ComponentFixture<FormDataLombaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDataLombaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormDataLombaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
