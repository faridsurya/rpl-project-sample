import { Component, OnInit, Input } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastService } from 'src/app/services/toast.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-form-data-lomba',
  templateUrl: './form-data-lomba.page.html',
  styleUrls: ['./form-data-lomba.page.scss'],
})
export class FormDataLombaPage implements OnInit {
  @Input() eventID: string;
  @Input() data: any;
  @Input() competitionID: any;
  constructor(
    public db: AngularFirestore,
    public auth: AngularFireAuth,
    public toast: ToastService,
    public modalCtrl: ModalController
  ) { }

  userdata: any = {};
  ngOnInit() {
    if(this.competitionID == undefined && this.data == undefined)
    {
      this.data = {pic:[]};
    }
    this.auth.onAuthStateChanged(user=>{
      this.userdata = user;
    })
  }

  

  loading: boolean;
  saveCompetition()
  {
    this.loading = true;
    
    var doc;
    if(this.competitionID == undefined)
    {
      doc = new Date().getTime().toString();
      this.data.author = this.userdata.email;
      this.data.eventID = this.eventID;
      this.data.partisipants = [];
      this.data.medal_gold_total =  this.data.medal_gold_total == undefined ? 1 : this.data.medal_gold_total;
      this.data.medal_silver_total = this.data.medal_silver_total == undefined ? 1 : this.data.medal_silver_total;
      this.data.medal_bronze_total = this.data.medal_bronze_total == undefined ? 1 : this.data.medal_bronze_total;
    }
    else{
      doc = this.competitionID;
    }
   
    this.db.collection('competitions').doc(doc).set(this.data).then(res=>{
      this.loading = false;
      this.toast.present('Lomba berhasil disimpan','middle');
    })
  }

  loadingMember: boolean;
  email_input: any;
  users: any ={};
  addMember(email)
  {
    this.loadingMember = true;
    this.db.collection('users').doc(email).get().subscribe(res=>{
      this.loadingMember = false;
      //console.log(res.data());
      if(res.data() == undefined){
        this.toast.present('User dengan email tersebut belum terdaftar pada aplikasi','middle');
        return;
      }else{       
        this.users[email] = res.data();
        if(this.data.pic == undefined) this.data.pic = [];
        this.data.pic.push(email);
        this.toast.present('Penanggungjawab berhasil ditambahkan','middle');
        this.email_input = '';
        //update
        this.db.collection('competitions').doc(this.competitionID).update({
          pic: this.data.pic
        });
      };
      
    });
    
  }

  dismiss()
  {
    this.modalCtrl.dismiss();
  }

}
