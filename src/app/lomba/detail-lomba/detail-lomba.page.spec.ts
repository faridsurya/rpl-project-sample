import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailLombaPage } from './detail-lomba.page';

describe('DetailLombaPage', () => {
  let component: DetailLombaPage;
  let fixture: ComponentFixture<DetailLombaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailLombaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailLombaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
