import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailLombaPageRoutingModule } from './detail-lomba-routing.module';

import { DetailLombaPage } from './detail-lomba.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailLombaPageRoutingModule
  ],
  declarations: [DetailLombaPage]
})
export class DetailLombaPageModule {}
