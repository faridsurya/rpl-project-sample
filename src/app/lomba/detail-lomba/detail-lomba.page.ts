import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormPendaftaranPage } from '../form-pendaftaran/form-pendaftaran.page';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormDataLombaPage } from '../form-data-lomba/form-data-lomba.page';
import { DetailPesertaPage } from '../detail-peserta/detail-peserta.page';
import * as firebase from 'firebase';
@Component({
  selector: 'app-detail-lomba',
  templateUrl: './detail-lomba.page.html',
  styleUrls: ['./detail-lomba.page.scss'],
})
export class DetailLombaPage implements OnInit {

  constructor(
    public modalController: ModalController,
    public routes: ActivatedRoute,
    public db: AngularFirestore,
    public auth: AngularFireAuth
  ) { }

  eventID:any;
  competitionID: any;
  userdata: any = {};
  ngOnInit() {
    this.competitionID = this.routes.snapshot.paramMap.get('ID');
    this.auth.onAuthStateChanged(res=>{
      this.userdata = res;      
      this.getCompetitionData();      
    })
    
  }
  selectedSegment: any = 'peserta';

  isCommittee:boolean;
  getEventData()
  {
    this.db.collection('events').doc(this.eventID).get().subscribe(res=>{
      var data = res.data();
      var id = res.id;
      this.getIsCommittee(data);
    })
  }

  getIsCommittee(data)
  {    
    if(data.committees == undefined) return;
    if(data.committees.includes(this.userdata.email)) this.isCommittee = true;
    console.log(this.isCommittee);
  }  

  async detailPeserta(data) {   
    if(!this.isPic) return;
    const modal = await this.modalController.create({
      component: DetailPesertaPage,
      cssClass: 'my-custom-class',
      componentProps:{data: data, competitionData: this.competition}
    });
    return await modal.present();
  }


  partisipants: any = [];
  getPartisipants()
  {
    this.partisipants = [];
    this.db.collection('partisipants',ref=>{
      return ref.where('competitionID','==',this.competitionID)
      .orderBy('score','desc');
    }).valueChanges({idField: 'id'}).subscribe(res=>{
      this.partisipants = res;      
      this.parsePartisipants(res);   
    })
  }

  competition: any = {};  
  getCompetitionData()
  {
    this.db.collection('competitions').doc(this.competitionID).valueChanges().subscribe(res=>{
      this.competition = res;
      this.getPartisipants();
      this.getEventID(res);
      this.getRole(res);
    });
  }

  isPic: boolean;
  getRole(data)
  {
    if(data.pic == undefined) return;
    if(data.pic.length == 0) return;
    if(data.pic.includes(this.userdata.email)) this.isPic = true;
    //get detail pic
    for(var i=0; i<data.pic.length; i++)
    {
      if(this.users[data.pic[i]] == undefined) this.getUser(data.pic[i]);
    }
  }

  getEventID(data)
  {
    this.eventID = data.eventID;
    this.getEventData();
  }

  gold:any = [];
  silver:any = [];
  bronze:any = [];

  users: any = {};
  parsePartisipants(data)
  {
    this.gold = [];
    this.silver = [];
    this.bronze = [];
    for(var i=0; i<data.length; i++)
    {
      var email = data[i].author;
      if(this.users[email] == undefined) this.getUser(email);      
    }
    //medal
    var totalG = this.competition.medal_gold_total;
    var totalS = this.competition.medal_silver_total;
    var totalB = this.competition.medal_bronze_total;
    //console.log(this.competition.medal_silver_total);
    if(data.length > this.gold)
    this.calulateMedal(data,this.gold,0,totalG);
    if(data.length > totalG+totalS)
    this.calulateMedal(data,this.silver,totalG, totalG+totalS);
    if(data.length > totalG+totalS+totalB)
    this.calulateMedal(data,this.bronze,totalS+totalG, totalG+totalS+totalB);
  }

  //calculate medal
  calulateMedal(data,medalArray, start, end)
  {
    for(var i = start; i < end; i++)
    {
      if(data[i].score != 0)
      medalArray.push(data[i]);
    }
  }

  loadingValidate:any = {};
  validateMedal(data,medal)
  {    
    this.loadingValidate[data.id] = true;
    var dt = {};
    dt[medal] = firebase.default.firestore.FieldValue.arrayUnion(data.id);    
    this.db.collection('competitions').doc(this.competitionID).update(dt).then(res=>{
      this.loadingValidate = {};
    });
    this.db.collection('partisipants').doc(data.id).update({medal:medal});
  }

  getUser(email)
  {
    this.db.collection('users').doc(email).get().subscribe(res=>{
      this.users[email] = res.data();
    });
  }
  







  async aturLomba() {
    const modal = await this.modalController.create({
      component: FormDataLombaPage,
      cssClass: 'my-custom-class',
      componentProps: {eventID: this.competition.eventID, competitionID: this.competitionID, data: this.competition}
    });
    return await modal.present();
  }
  
}
