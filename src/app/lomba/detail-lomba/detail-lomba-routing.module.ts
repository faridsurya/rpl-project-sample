import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailLombaPage } from './detail-lomba.page';

const routes: Routes = [
  {
    path: '',
    component: DetailLombaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailLombaPageRoutingModule {}
