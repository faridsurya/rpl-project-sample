import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImageUploderPage } from './image-uploder.page';

const routes: Routes = [
  {
    path: '',
    component: ImageUploderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImageUploderPageRoutingModule {}
