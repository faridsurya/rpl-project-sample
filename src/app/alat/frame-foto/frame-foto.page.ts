import { Component, OnInit } from '@angular/core';

import { ModalController } from '@ionic/angular';
import { ImageUploderPage } from 'src/app/image-uploder/image-uploder.page';
import domtoimage from 'dom-to-image';
import { PilihFramePage } from '../pilih-frame/pilih-frame.page';
import { Plugins, CameraResultType, Capacitor,CameraPhoto, CameraSource, FilesystemEncoding } from '@capacitor/core';
import { File } from '@ionic-native/file/ngx';
const { Camera, Share, Filesystem, FilesystemDirectory,  Device, useFileSystem } = Plugins;




@Component({
  selector: 'app-frame-foto',
  templateUrl: './frame-foto.page.html',
  styleUrls: ['./frame-foto.page.scss'],
})
export class FrameFotoPage implements OnInit {

  device: any;
  constructor(
    private modalController: ModalController,
    public file: File
  ) { }

  ngOnInit() {
    this.getDeviceInfo();
  }

  async getDeviceInfo()
  {
    this.device = await Device.getInfo();
    console.log(this.device);
  }

  showDownload: boolean;
  updatingProfileImage: boolean;
  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Uri
    });
    var imageUrl = image.webPath;

    this.savePicture(image);

    const modal = await this.modalController.create({
      component: ImageUploderPage,
      cssClass: 'my-custom-class',
      componentProps:{imageData:imageUrl,imagePath: false,ratio:1,width:700,height:700}
    }); 
    modal.onDidDismiss().then(res=>{
      if(res.data.imageUrl != false)
      {
        var img = document.getElementById("photo") as HTMLImageElement;        
        img.src = res.data.imageUrl;
        this.showDownload = true;
      }
    });
    return await modal.present(); 
  }

  async pilihBingkai() {
    const modal = await this.modalController.create({
      component: PilihFramePage,
      cssClass: 'my-custom-class'
    }); 
    modal.onDidDismiss().then(res=>{
      if(res.data.url != undefined)
      {
        var fr = document.getElementById('frame') as HTMLImageElement;
        fr.src = res.data.url;     
      }
    });
    return await modal.present(); 
  }

  async downloadImage()
  {
    var device = this.device;
    var frame = document.getElementById('image-frame');
    frame.style.width = '800px';
    frame.style.height = '800px';
    var dataUrl = await domtoimage.toJpeg(frame, { quality: 1 })
    .then(dataUrl=> {     
       return dataUrl;
    }).catch(err=>{
      alert('gagal ambil data');
    })
    //alert(device.platform);
    //if(device.platform == 'web') this.saveToComputer(frame, dataUrl);
    //else 
    this.saveToComputer(frame, dataUrl);
    //this.saveToGallery(dataUrl,frame);
  }

  async saveToGallery(dataUrl,frame)
  {
    alert('saving to gallery');
    const base64Response = await fetch(dataUrl);
    const blob = await base64Response.blob();
    const res = await this.file.writeFile(this.file.dataDirectory,'olyq.jpeg',dataUrl).then(res=>{
      return res;
    });
    alert(JSON.stringify(res));
    frame.style.width = '100%';
    frame.style.height = null;
    let shareRet = Share.share({
      title: 'Virtual OLYQ 2021',
      text: 'Olyq segera hadir lagi! Ayo sertakan putra putri Anda untuk berkompetisi.',
      url: res.native,
      dialogTitle: 'VIRTUAL OLYQ 2021'
    });   
  }



  


  saveToComputer(frame, dataUrl)
  {
    var link = document.createElement('a');
    link.download = 'olyq_virtual.jpeg';
    link.href = dataUrl;
    link.click();
    frame.style.width = '100%';
    frame.style.height = null;
    
  }

  async share()
  {
    const dataimg = await domtoimage.toJpeg(document.getElementById('image-frame'), { quality: 1 })
    .then(function (dataUrl) {
     return dataUrl;
    });
    const base64Response = await fetch(dataimg);
    const blob = await base64Response.blob();
    var blobUrl = URL.createObjectURL(blob);
    
    //console.log(blobUrl);
    var savedPic = await this.savePicture(dataimg);
    //console.log(savedPic);
    let shareRet = Share.share({
      title: 'Virtual OLYQ 2021',
      text: 'Olyq segera hadir lagi! Ayo sertakan putra putri Anda untuk berkompetisi.',
      url: blobUrl,
      dialogTitle: 'VIRTUAL OLYQ 2021'
    });    
   
  }

  private async savePicture(cameraPhoto: CameraPhoto) {
    // Convert photo to base64 format, required by Filesystem API to save
    const base64Data = await this.readAsBase64(cameraPhoto);
  
    // Write the file to the data directory
    const fileName = new Date().getTime() + '.jpeg';
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: FilesystemDirectory.Data
    });
  
    // Use webPath to display the new image instead of base64 since it's
    // already loaded into memory
    return {
      filepath: fileName,
      webviewPath: cameraPhoto.webPath
    };
  }

  private async readAsBase64(cameraPhoto: CameraPhoto) {
    // Fetch the photo, read as a blob, then convert to base64 format
    const response = await fetch(cameraPhoto.webPath!);
    const blob = await response.blob();
  
    return await this.convertBlobToBase64(blob) as string;  
  }
  
  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });


  async fileWrite() {
    Filesystem.writeFile({
      path: 'secrets/text.txt',
      data: "This is a test",
      directory: FilesystemDirectory.Documents,
      encoding: FilesystemEncoding.UTF8
    }).then(res=>{
      alert('success:'+JSON.stringify(res));
    }).catch(err=>{
      alert(JSON.stringify(err));
    });
  }

  shareFile(data)
  {
    let shareRet = Share.share({
      title: 'Virtual OLYQ 2021',
      text: 'Olyq segera hadir lagi! Ayo sertakan putra putri Anda untuk berkompetisi.',
      url: data,
      dialogTitle: 'VIRTUAL OLYQ 2021'
    });   
  }
  

}
