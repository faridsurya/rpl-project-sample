import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FrameFotoPage } from './frame-foto.page';

const routes: Routes = [
  {
    path: '',
    component: FrameFotoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrameFotoPageRoutingModule {}
