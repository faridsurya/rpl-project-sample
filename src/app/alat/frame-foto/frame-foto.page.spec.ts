import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FrameFotoPage } from './frame-foto.page';

describe('FrameFotoPage', () => {
  let component: FrameFotoPage;
  let fixture: ComponentFixture<FrameFotoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameFotoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FrameFotoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
