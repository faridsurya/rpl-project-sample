import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FrameFotoPageRoutingModule } from './frame-foto-routing.module';

import { FrameFotoPage } from './frame-foto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FrameFotoPageRoutingModule
  ],
  declarations: [FrameFotoPage]
})
export class FrameFotoPageModule {}
