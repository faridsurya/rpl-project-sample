import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PilihFramePage } from './pilih-frame.page';

describe('PilihFramePage', () => {
  let component: PilihFramePage;
  let fixture: ComponentFixture<PilihFramePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilihFramePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PilihFramePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
