import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PilihFramePage } from './pilih-frame.page';

const routes: Routes = [
  {
    path: '',
    component: PilihFramePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PilihFramePageRoutingModule {}
