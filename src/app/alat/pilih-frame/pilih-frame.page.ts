import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-pilih-frame',
  templateUrl: './pilih-frame.page.html',
  styleUrls: ['./pilih-frame.page.scss'],
})
export class PilihFramePage implements OnInit {

  constructor(
    private modalCtrl : ModalController
  ) { }

  ngOnInit() {
    this.getFrames();
  }

  frames: any = [];

  getFrames()
  {
    fetch('./assets/frames/frames.json').then(response => response.json())
    .then(json => {
      this.frames = json;
    }   
    );
  }

  

  pilih(data)
  {
    this.modalCtrl.dismiss({
      url: data.url
    })
  }

  dismiss()
  {
    this.modalCtrl.dismiss();
  }

}
