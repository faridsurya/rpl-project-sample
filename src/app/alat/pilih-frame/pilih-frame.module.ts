import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PilihFramePageRoutingModule } from './pilih-frame-routing.module';

import { PilihFramePage } from './pilih-frame.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PilihFramePageRoutingModule
  ],
  declarations: [PilihFramePage]
})
export class PilihFramePageModule {}
